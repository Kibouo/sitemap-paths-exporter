# Burp need to load each class explicitly
from burp import IBurpExtender, IContextMenuFactory
from java.util import ArrayList
from javax.swing import JMenuItem, JFileChooser
from functools import reduce
from os import linesep

# This allow us to get typing hints for all burp classes in our IDE
from burp import *


class BurpExtender(IBurpExtender, IContextMenuFactory):
    _callbacks = None
    _helpers = None

    _context = None

    def registerExtenderCallbacks(self, callbacks):
        self._callbacks = callbacks
        self._helpers = callbacks.getHelpers()
        self._context = None

        self._callbacks.setExtensionName("Sitemap Paths Exporter")
        self._callbacks.registerContextMenuFactory(self)

        print("Plugin loaded")


    def createMenuItems(self, IContextMenuInvocation):
        self._context = IContextMenuInvocation

        if self._context.getInvocationContext() != self._context.CONTEXT_TARGET_SITE_MAP_TREE:
            return None

        menu_list = ArrayList()
        menu_list.add(JMenuItem("Export filenames", actionPerformed=self._export_filenames))
        menu_list.add(JMenuItem("Export dir parts", actionPerformed=self._export_dir_parts))
        menu_list.add(JMenuItem("Export path parts", actionPerformed=self._export_path_parts))
        menu_list.add(JMenuItem("Export paths", actionPerformed=self._export_paths))
        return menu_list


    def _export_filenames(self, _):
        urls = self._selected_sitemap_urls()
        nested_paths = map(lambda url: self._paths_in_sitemap_for_url(url), urls)
        paths = reduce(lambda acc, paths: acc + paths, nested_paths, [])

        filenames = []
        for split_path in map(lambda path: path.split('/'), paths):
            if '.' in split_path[-1]:
                filenames.append(split_path[-1])
        filenames = set(filenames)

        self._save_text_to_file(map(lambda filename: filename + linesep, filenames))


    def _export_dir_parts(self, _):
        urls = self._selected_sitemap_urls()
        nested_paths = map(lambda url: self._paths_in_sitemap_for_url(url), urls)
        paths = reduce(lambda acc, paths: acc + paths, nested_paths, [])

        dir_parts = []
        for split_path in map(lambda path: path.split('/'), paths):
            dir_parts += split_path[:-1]
            if '.' not in split_path[-1]:
                dir_parts.append(split_path[-1])
        dir_parts = set(dir_parts)

        self._save_text_to_file(map(lambda part: part + linesep, dir_parts))


    def _export_path_parts(self, _):
        urls = self._selected_sitemap_urls()
        nested_paths = map(lambda url: self._paths_in_sitemap_for_url(url), urls)
        paths = reduce(lambda acc, paths: acc + paths, nested_paths, [])
        words = reduce(lambda acc, words: acc + words, map(lambda path: path.split('/'), paths), [])
        words = set(words)
        self._save_text_to_file(map(lambda word: word + linesep, words))


    def _export_paths(self, _):
        urls = self._selected_sitemap_urls()
        nested_paths = map(lambda url: self._paths_in_sitemap_for_url(url), urls)
        paths = reduce(lambda acc, paths: acc + paths, nested_paths, [])
        paths = set(paths)
        self._save_text_to_file(map(lambda path: path + linesep, paths))


    def _selected_sitemap_urls(self):
        selected = []
        for message in filter(lambda m: m.getHttpService(), self._context.getSelectedMessages()):
            service = message.getHttpService()
            selected.append("{}://{}".format(service.getProtocol(), service.getHost()))
        return selected


    def _paths_in_sitemap_for_url(self, url):
        paths = []
        for message in self._callbacks.getSiteMap(url):
            first_line_of_http = self._helpers.analyzeRequest(message.getHttpService(), message.getRequest()).getHeaders()[0]

            path_with_params = ''.join(first_line_of_http.split(' ')[1:-1])
            path = path_with_params.split('?')[0]

            paths.append(path)
        return list(set(paths))


    def _save_text_to_file(self, lines_of_content):
        file_chooser = JFileChooser()
        if file_chooser.showSaveDialog(None) == JFileChooser.APPROVE_OPTION:
            with open(file_chooser.getSelectedFile().getPath(), 'w') as save_file:
                save_file.writelines(lines_of_content)